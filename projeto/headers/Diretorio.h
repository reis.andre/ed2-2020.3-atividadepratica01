/*
 * Arquivo Base da Classe Diretório
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004AC
 * Data de Criação:  03/02/2021
*/

#ifndef DIRETORIO_H
#define DIRETORIO_H

#include <iostream>
#include <map> 
#include <string>
#include "Balde.h"

using namespace std;

class Diretorio {
    private:
    int profGlobal; // Profundidade Global
    int tamChave, tamBalde; // tamanho da chave e do balde, a ser definido pelo usuário
    map<string, Balde*>* baldes; 
    
    bool validaKey(string key);
     // id identificar do balde q deu ruim


    // inserir, buscar, dividir baldes
    // duplicar diretório
  
    public:      
    Diretorio(int tamBalde, int bitsKey);
    ~Diretorio();
   
    bool insereKey(string key);
    bool buscaKey(string key);
    void duplicarBalde(string id, Balde* balde);
    void duplicarDiretorio();
    void imprimirDiretorio();
    void imprimirAnalise();
};

#endif // DIRETORIO_H
