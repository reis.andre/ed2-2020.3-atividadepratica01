/*
 * Arquivo Base da Classe Balde
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004AC
 * Data de Criação:  03/02/2021
*/

#ifndef BALDE_H
#define BALDE_H 

#include <iostream>
#include <string>
#include <set>

using namespace std;


class Balde {
    private:
    int profLocal; // ProfundidadeLocal       
    int tam; // tamanho do balde
    set<string>* chaves; // não sei se o unordered_set seria melhor
    
    public:
    Balde(int x);
    Balde(int x, int prof);
    ~Balde();
    int inserir(string key);
    int getProf();
    int getTam();
    int getTamBytesChaves();
    bool buscar(string key);
    void imprimirBalde();
    void setProf(); // aumentar a profundidade em 1 unidade 
    void redistribuirChaves(string chave, Balde* destino);

};

#endif // BALDE_H
