/*
 * Arquivo Base da Classe Gerador de Chave
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004AC
 * Data de Criação:  10/02/2021
*/

#ifndef GERADORCHAVE_H
#define GERADORCHAVE_H

#include <iostream>
#include <map> 
#include <string>
#include "Balde.h"
#include "Diretorio.h"

using namespace std;

class GeradorChave {
    private:
  
    public:      
    GeradorChave();
    void gerarChave(int qnt, int bitsKey, Diretorio* d);
    void gerarChavePadrao(int qnt, int bitsKey, string padraoInicial, Diretorio* d);
   
};

#endif // GERADORCHAVE_H
