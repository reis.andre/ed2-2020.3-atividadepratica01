/*
 * Arquivo de Implementação da Classe Gerador de Chabe
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004AC
 * Data de Criação:  10/02/2021
*/

#include "./headers/GeradorChave.h"
#include <math.h>
#include <algorithm>

GeradorChave::GeradorChave(){
}

void GeradorChave::gerarChave(int qnt, int bitsKey, Diretorio* d){

    if(pow(2,bitsKey) < qnt){
        throw invalid_argument("Impossivel gerar a qnt necessarias de chaves solicitas pelos parametros");
    }

    set<string> chaves;

    for(int i=0; i < qnt; i++){

        int j = 0;
        string key = "";

        while( j < bitsKey){
            rand() % 2? key+= "0" : key+= "1";
            j++;
            //if(key.replace(,))
        }

        replace( key.begin(), key.end(), '2', '1');  // teste replaca
        
        //cout << "ERRO KEY: " << key << "i: " << i << endl;
        
        if(!d->insereKey(key)){
            //cout << "ERRO KEY: " << key << "i: " << i << endl;
            i--;
        }
            
    }


}

void GeradorChave::gerarChavePadrao(int qnt, int bitsKey, string padraoInicial, Diretorio* d){

    if(pow(2,bitsKey-padraoInicial.size()) < qnt){
        throw invalid_argument("Impossivel gerar a qnt necessarias de chaves com padrao solicitas pelos parametros");
    }
        int i;
        for( i=0; i < qnt; i++){

        int j = 0;
        string key = padraoInicial;
        int fim = bitsKey - padraoInicial.size();

        while( j < fim){
            rand() % 2? key+= "0" : key+= "1"; // verificar se a soma pode causa erro de caracter e surgir 2 inicial
            j++;
        }
        
        replace( key.begin(), key.end(), '2', '1');  // teste replece, começar a aparecer 2 na string

        if(!d->insereKey(key))
            i--;
    }


}