/*
 * Arquivo de Implementação da Classe Balde
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004AC
 * Data de Criação:  03/02/2021
*/

#include "./headers/Balde.h"

//Construtor do Balde Inicial
Balde::Balde(int x){
    this->tam = x;
    this->profLocal = 0;
    this->chaves = new set<string>();
}

//Construtor para Duplicar Balde
Balde::Balde(int x, int prof){
    this->tam = x;
    this->profLocal = prof;
    this->chaves = new set<string>();
}

//Destrutor do Balde
Balde::~Balde(){
    //cout << "Excluindo Balde" << endl;
     delete this->chaves;
}

//Funcao que retorna a dlocal do balde
int Balde::getProf(){
    return this->profLocal;
}

//Funcao que retorna a qnt de chaves no balde
int Balde::getTam(){
    return this->chaves->size();
}

//Funcao que retorna o tamanho da viável chaves do balde
int Balde::getTamBytesChaves(){
    return sizeof(this->chaves->size());
}

//Funcao que aumenta em 1 unidade a dlocal
void Balde::setProf(){
    this->profLocal++;
}

//Funcao para inserir chave no balde
int Balde::inserir(string key){
    if(this->buscar(key)){ // TALVEZ O ERRO ESTEJA AQUI
        return -2; // CHAVE JÁ EXISTE  
    }else if(chaves->size() == this->tam)
        return -1; // LOTADO
    else{
        chaves->insert(key);
        return 0; // DEU BOM
    }
    
}

//Funcao para buscar chave no balde
bool Balde::buscar(string key){
    for (auto it=chaves->begin(); it!=chaves->end(); ++it){
        if(key == *it)
            return true;
    }
    return false;
}

//Funcao para imprimir as chaves do balde
void Balde::imprimirBalde(){
    cout << " Prof.: " << this->profLocal << " Chaves: ";
    for (std::set<string>::iterator it=chaves->begin(); it!=chaves->end(); ++it){
            cout << *it << " | ";
    }
    cout << endl;
}

//Funcao para redistribuir as chaves entre o balde atual e o de destino conforme chave de bits iniciais
void Balde::redistribuirChaves(string chave, Balde* destino){
    
    set<string> aux;

    for (auto it = chaves->cbegin(); it != chaves->cend() /* not hoisted */; /* no increment */)
    {
        if ((*it).substr(0,profLocal) == chave){
            //destino->inserir(*it);
            //it = chaves->erase(it); // C++11, before chaves->erase(it++)
            //chaves->erase(it++);    // or "it = m.erase(it)" since C++11

            aux.insert(*it);
            ++it;
        }
        else
            ++it;
        
    }

    for (std::set<string>::iterator it=aux.begin(); it!=aux.end(); ++it){
        destino->inserir(*it);
        this->chaves->erase(*it);
    }

}