/*
 * Arquivo principal do projeto
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 * Data de Criação:  03/02/2021
*/
//#include <time.h>
//#include <chrono>
#include "./headers/Diretorio.h"
#include "./headers/GeradorChave.h"
#include <iomanip>
//#include <conio.h>

int main(int argc, char *argv[])
{
    int m,n,b, impressao, analise;
    string padrao;
    Diretorio* dAletorio;
    Diretorio* dPadrao;
    GeradorChave gerador;
    
    srand(time(NULL)); //seta a semente de tempo para ser aleatorio

    // Verifica se existe algum parâmetros
    if (argc != 1)
    {
        cout << "Nao eh necessario passar parametros" << endl;
        exit(1);
    }

   try{

       cout << "*** Iniciando o Programa ***" << endl;

     /* solicitar ao usuário o tamanho M a ser usado para os baldes
     número de bits B a ser usado para as pseudo-chaves. */

    //m = 6; b =8; n =10; padrao = "1"; impressao = 1; analise = 1;

    
       cout << "Informe o tamanho M dos baldes: "; cin >> m;
        while(m<1){
            cout << "Invalido - Informa o tamanho M dos baldes: "; cin >> m;
        }

        cout << "Informe a quantidade B de bits para as chaves [1,10]: "; cin >> b;
        while (b < 1 || b > 10){ // acima de 10 dá erro ao gerar as chaves, não entendi
            cout << "Invalido - Informa a quantidade B de bits para as chaves: "; cin >> b;
        }
        
        cout << "Informe a quantidade N de insercoes para teste: "; cin >> n;
        while (n < 1){
            cout << "Invalido - Informa a quantidade N de insercoes para teste: "; cin >> n;
        }

        cout << "Informe o padrao de bits iniciais (Ex.: 1001) para teste: "; cin >> padrao;
        while (padrao.size() > b -1){ // validação simples, talvez melhore
            cout << "Invalido - Informa o padrao de bits inicias para teste: "; cin >> padrao;
        }

        cout << "Deseja Imprimir os diretorios e chaves? 0-Nao | 1 - Sim : "; cin >> impressao;
        while (impressao != 0 && impressao != 1){ // validação simples, talvez melhore
            cout << "Invalido - 0-Nao | 1 - Sim : "; cin >> impressao;
        }

        cout << "Deseja Imprimir analises? 0-Nao | 1 - Sim : "; cin >> analise;
        while (analise != 0 && analise != 1){ // validação simples, talvez melhore
            cout << "Invalido - 0-Nao | 1 - Sim : "; cin >> analise;
        }
    
        getchar();
    
     //   cout << m << " " << b << " " << n << " " << padrao << endl;

        dAletorio = new Diretorio(m,b); // tamanho m dos baldos e b bits para as chaves
        dPadrao = new Diretorio(m,b);

        //TESTES
        gerador.gerarChave(n,b,dAletorio); // inserindo n chaves aleatorias
        if(impressao == 1){
            cout << endl << "# Diretorio Aleatorio" << endl;
            dAletorio->imprimirDiretorio();
        } 

        gerador.gerarChavePadrao(n,b,padrao,dPadrao); // inserindo n chaves aleatorias com padrao inicial igual
        
        if(impressao == 1){
            cout << "# Diretorio Aleatorio com Padrao" << endl;
            dPadrao->imprimirDiretorio();
        } 

        //Análise
        cout << endl << " # Resultados Analise Intersecao Aleatoria" << endl;
        if(analise == 1) dAletorio->imprimirAnalise();
        
    
        
        cout << endl << " # Resultados Analise Intersecao com padrao inicial" << endl;
        if(analise == 1) dPadrao->imprimirAnalise();

        delete dAletorio; delete dPadrao;

   }catch(const std::invalid_argument& e){
       cout << "### ERROR ###" << endl;
       std::cerr << e.what() << '\n';

        delete dAletorio; delete dPadrao;
   }
   catch(const std::exception& e){
       cout << "### ERROR ###" << endl;
       std::cerr << e.what() << '\n';
   }

    cout << endl << "*** Encerando o Programa ***" << endl;
   
    return 0;
}