/*
 * Arquivo de Implementação da Classe Diretório
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004AC
 * Data de Criação:  03/02/2021
*/

#include "./headers/Diretorio.h"
#include <iostream>
#include <set>
#include <queue>
#include <iomanip>
#include <math.h>


//Construtor do Diretorio - Tamanho do Baldo e Numero de bits para a chave
Diretorio::Diretorio(int tamBalde, int bitsKey){
    this->tamBalde = tamBalde;
    this->tamChave = bitsKey;
    this->profGlobal = 1; // 2^1 posições iniciais
    baldes = new map<string, Balde*>(); // criando mapa do diretorio
    Balde* baldeInicial = new Balde(tamBalde); // balde inicial e único
    baldes->insert(pair<string, Balde*>("0", baldeInicial)); // aponta 2^d=2^1=2 posicoes pro mesmo balde
    baldes->insert(pair<string, Balde*>("1", baldeInicial)); 
}

// Destrutor do Diretorio
Diretorio::~Diretorio(){


    //deletar alocações
    //cout << "excluindo diretorio" << endl;
    //delete this->baldes;
        set<Balde*> baldesGeral;

        //delete baldes->at("0");
        //cout << baldes->at("1") << endl;
        //delete baldes->at("1");

        for (std::map<string,Balde*>::iterator it=baldes->begin(); it!=baldes->end(); ++it){
           //baldes->erase(it);
            //delete (it->second);
            baldesGeral.insert(it->second);
        }
        
        
        //cout << "Primeiro for fim" << endl;
        

        for (std::set<Balde*>::iterator it=baldesGeral.begin(); it!=baldesGeral.end(); ++it){
            //baldesGeral.erase(it);
            delete *it;
        }
            
        

       //cout << baldesGeral. << endl;
        //delete baldes->at("1");

    delete this->baldes;

}

//Funcao para validação simplificada da chave
bool Diretorio::validaKey(string key){
    
    if(key.size() == this->tamChave) // valida somente o tamanho, nao a chave como um todo
        return true;
    else{
        cout << key <<" - Chave Invalida - Operacao nao realizada" << endl;
        return false;
    }   
}

//Funcao para Imprimir o Diretorio e as chaves em cada balde para testes
void Diretorio::imprimirDiretorio(){
    set<Balde*> baldesGeral;

    cout << "ProfGlobal: "  << this->profGlobal << endl;
    
    for (auto it=baldes->begin(); it!=baldes->end(); ++it){
            
            if(baldesGeral.find(it->second) == baldesGeral.end()){
                cout << "Diretorio: "  << it->first << " ";
                it->second->imprimirBalde();
            } baldesGeral.insert(it->second);

    }
    cout << endl;
}

//Funcao para Inserir uma chave no diretorio
bool Diretorio::insereKey(string key){

    if(validaKey(key)){
        string id = key.substr(0,this->profGlobal);
        Balde* baldeAux = baldes->find(id)->second;
        if(baldeAux == baldes->end()->second){ // verifica se o balde existe
                //cout << id << " - Balde nao encontrado" << endl;
                cout << "Key: " << key << "Prof Global: " << profGlobal << endl;
                //this->imprimirDiretorio();
                throw invalid_argument(id + " - Balde nao encontrado");
                return false;
        }else{ // caso exista, tenta inserir a chave
            int i = baldeAux->inserir(key);
            if(i == -2){ // erro de chave ja existente
                return false;
            }
            else if(i == -1){ // erro diretorio cheio
                //cout << "ProfGlobal: " << this->profGlobal << " ProfLocal: " << baldeAux->getProf() <<endl;
                if(this->profGlobal == baldeAux->getProf()){   /// dlocal = dglobal  
                    this->duplicarDiretorio(); // duplica o diretorio
                    this->duplicarBalde(id, baldeAux);
                    //this->imprimirDiretorio();
                    return this->insereKey(key); // tenta inserir a chave novamente e retorna o resultado da operacao
                }else{ // dlocal < dglobal
                    //cout << "LOTADO - FAZER DIVISAO DO BALDE " << id << endl;
                    this->duplicarBalde(id, baldeAux); // duplica o balde
                    return this->insereKey(key); // tenta inserir a chave novamente e retorna o resultado da operacao
                }
            }else if(i == 0){
                //cout << key << " - Insercao feita com sucesso" << endl;
                return true;
            }else return false;
        }
    }else
        return false;

}

//Funcao para buscar a chave no Diretorio
bool Diretorio::buscaKey(string key){

    if(validaKey(key)){
        string id = key.substr(0,this->profGlobal);
        Balde* baldeAux = baldes->find(id)->second;
        if(baldeAux == baldes->end()->second){
                cout << "Balde nao encontrado" << endl;
                return false;
        }else{
            return baldeAux->buscar(key);
        }
    }else
        return false;

}

//Funcao para Duplicar o Balde Cheio quando dlocal < dglobal
void Diretorio::duplicarBalde(string id, Balde* baldeAux){
    int profAnterior = baldeAux->getProf();
    
    string idNovoBalde = *(id.substr(0,profAnterior).end()) == 0 ? id.substr(0,profAnterior) + "1" : id.substr(0,profAnterior) + "0";
    // idNovoBalde

    Balde* novoBalde = new Balde(this->tamBalde, profAnterior + 1);
   
    if(idNovoBalde.size() != this->profGlobal){ // caso que temos mais que 2 baldes na operação

        queue<string> fila;
        //Balde* novoBalde = new Balde(this->tamBalde, profAnterior + 1);

        fila.push(idNovoBalde);
        int iteracoes = 1;

        for(int i=idNovoBalde.size(); i < this->profGlobal; i++){ // gerando as chaves corretas
            
            for(int j = 0; j < iteracoes; j++){
                string aux = fila.front(); fila.pop();
                fila.push(aux + "0");
                fila.push(aux + "1");
            } iteracoes++;

        }

        while(!fila.empty()){ // ajustando o mapa
            auto it = baldes->find(fila.front()); fila.pop(); // encontra o diretorio, tira elemento da fila
            if (it != baldes->end()) // TIREI // ALTERA O APONTAMENTO PRO NOVO BALDE
                it->second = novoBalde; // NOVO TESTE
        } // como todos apontam pra um mesmo balde, alterar a profundidade dos dois baldes envolvidos


    }else{ // apenas 2 baldes envolvidos, código abaixo normalmente
        auto it = baldes->find(idNovoBalde); 

        if (it != baldes->end()) // TIREI
        it->second = novoBalde; // NOVO TESTE

        
    }

    baldeAux->setProf();
    
    baldeAux->redistribuirChaves(idNovoBalde,novoBalde);
}

//Funcao para Duplicar o Diretorio Cheio quando dlocal = dglobal
void Diretorio::duplicarDiretorio(){
    
    if(this->tamChave == this->profGlobal){
        this->imprimirDiretorio();
        throw invalid_argument("Impossivel duplicar diretorio, tamKey == dGlobal");
    }
        

    map<string, Balde*>* baldesNovo = new map<string, Balde*>(); // criando novo mapa do diretorio
    string aux; // ganhar tempo
    //Balde* baldeAux;

    for (auto it=baldes->begin(); it!=baldes->end(); ++it){
        // passando pelo antigo que  gerará as 2 novas posições add 0 e 1 no fim, ganhando tempo na geração
            
            aux = it->first;
            baldesNovo->insert(pair<string, Balde*>(aux + "0", it->second));
            baldesNovo->insert(pair<string, Balde*>(aux + "1" , it->second));
            //baldes->insert(pair<string, Balde*>("1", baldeAux2)); 
            //cout << "Diretorio: "  << it->first << endl;
    }

    delete this->baldes; // deletando diretorio anterior
    this->baldes = baldesNovo; // apontando pro novo diretorio

    this->profGlobal++; // aumentado a dGlobal, o que permitirá o que segue-se
    //feito isso, o procedimento próximo se dará igual a duplicação de balde, basta entrar no método novamente

    //cout << "ProfGlobal " << profGlobal << endl;
    //cout << "Fim da Duplicacao de Diretorio" << endl;
}

//Funcao para imprimir as Analises solicitadas
void Diretorio::imprimirAnalise(){
    //Analise os resultados dos testes com relação ao fator de carga da tabela e à ocupação de memória.
    set<Balde*> baldesGeral;
    int cont = 0;
    int memoria = 0;

    for (std::map<string,Balde*>::iterator it=baldes->begin(); it!=baldes->end(); ++it){
        baldesGeral.insert(it->second);
    }
        
    for (std::set<Balde*>::iterator it=baldesGeral.begin(); it!=baldesGeral.end(); ++it){
        cont += (*it)->getTam();
        memoria+= (*it)->getTamBytesChaves();
        memoria += sizeof(*it);
    }


    memoria += sizeof(this->baldes);
    
    int posicoesTotal = pow(2,this->profGlobal)*this->tamBalde;
    int posicoesTotalReal = baldesGeral.size()*this->tamBalde;
    cout << "  Posicoes Ocupadas: " << cont << " | Prof. Global: " << this->profGlobal << " | Qnt Baldes: " << baldesGeral.size()
        << endl << "  Posicoes Totais Teo.: " << posicoesTotal
        << " | Fator de Carga Teo.: " << std::setprecision(2) << (float) cont / posicoesTotal * 100  << " %" << endl
        << "  Posicoes Totais Real:  " << posicoesTotalReal << " | Fator de Carga Real: "
        << std::setprecision(2) << (float) cont / posicoesTotalReal * 100  << " %" << endl
        << "Memoria Total: " << memoria << endl;
}
