# ED2-2020.3-AtividadePratica01

Atividade Prática de Implementação
DCC012 - Estrutura de Dados II
2020.3 - ERE - UFJF
14/02/2020
André Luiz dos Reis - 201965004AC

Instruções Iniciais:

Estrutura de arquivos:
1) projeto: contém todos os arquivos .cpp e .h da aplicação;

2) scripts:

    2.1 - run.cdm: (Windows) faz apenas a compilação do programa e inicia o executável 
    
    2.2 - run.sh: (Linux) faz apenas a compilação do programa e inicia o executável

Explicação do Método Main:

    * A quantidade de inserções será validada ante os parametros informados,
      gerando um erro caso não seja possível gerar a quantidade solicitada;

    * O padrão informado é uma sequencia de bits que será o padrão inicial das inserções para teste;

    * A impressão imprimirá apenas a numeração do primeiro diretório,
      caso exista duas ou mais posições que apontem para o mesmo balde;   

Caso houver dúvidas relacionadas à navegação ou execução do código, o responsável deverá ser consultado!